﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    Vector3 offset;
    public GameObject player;

	// Use this for initialization
	void Start () {

        offset = transform.position - player.transform.position; // subtracts the vectors

	}
	
	// LateUpdate is called after every other update is made, useful for positioning cameras
	void LateUpdate () {
        if (player != null)
            transform.position = player.transform.position + offset;
        
	}
}
