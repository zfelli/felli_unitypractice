﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;
    public float speed;

    int count = 0;

    public Text countText;
    public Text WinText;

    public Rigidbody floorRB;

    public bool dead = false;



	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>(); // Searches all components and returns a reference to selected; now "rb" will act as a variable for that component
	}
	
	// Update is called once per frame
	void Update () {
		if (dead = false && transform.position.y <= -5)
        {
            dead = true;
            GetComponent<MeshRenderer>().enabled = false;
            Invoke("RestartLevel", 2f);
        }
	}

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        print("restart level!");
    }

    private void FixedUpdate() // FixedUpdate is how many times physics are calculated
    {
        float moveHorizontal = -1 * Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveVertical, 0f, moveHorizontal); // creates a vector out of the previous two

        //rb.AddForce(movement * Time.deltaTime * speed); // adds force of created vector to the ball

        floorRB.AddTorque(movement * Time.deltaTime * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            count += 1;

            SetCountText();

            Destroy(other.gameObject);
            
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 12)
        {
            WinText.text = "A WINNER IS YOU";
        }
    }
 
}
